$(document).ready(function () {
    
    $("#submit-btn").on("click", function () {
        $("#modalClose").hide();
    });

    $("#modalSubmit").on("click", function () {
        $("#modalClose").show();
    });

    $("#modalClose").on("click", function () {
        $(this).hide();
    });

});