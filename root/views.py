from django.shortcuts import render
from django.views.generic import View
from django.urls import reverse
from django.views.generic.edit import CreateView
# Impot Models
from root.models import DogModel
from root.models import OwnerModel
# Import Forms
from root.forms import AddDogForm
from root.forms import AddOwnerForm

class IndexView(View):
    template_name = "index.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class FrontendView(View):
    template_name = "frontend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class BackendView(View):
    template_name = "backend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)

# DOG
def ListDog(request):
    dogs = DogModel.objects.all()
    return render (request, template_name='dog/list.html',context={'dogs':dogs})

class AddDog (CreateView):
    model = DogModel
    form_class = AddDogForm
    template_name = 'dog/add.html'

    def get_success_url(self):
        return reverse ('ListDog')

# OWNER
def ListOwner(request):
    owners = OwnerModel.objects.all()
    return render (request, template_name='owner/list.html',context={'owners':owners})

class AddOwner (CreateView):
    model = OwnerModel
    form_class = AddOwnerForm
    template_name = 'owner/add.html'

    def get_success_url(self):
        return reverse ('ListOwner')




