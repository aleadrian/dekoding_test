from django.urls import path

from .views import (
    IndexView,
    FrontendView,
    BackendView,
    AddDog,
    AddOwner
)

from root import views

from root.models import DogModel
from root.models import OwnerModel


urlpatterns = [
    path('', IndexView.as_view(), name="index"),
    path('frontend/', FrontendView.as_view(), name="frontend"),
    path('backend/', BackendView.as_view(), name="backend"),
    path('dog/list/', views.ListDog, name="ListDog"),
    path('dog/add/', AddDog.as_view(), name="AddDog"),
    path('owner/list/', views.ListOwner, name="ListOwner"),
    path('owner/add/', AddOwner.as_view(), name="AddOwner"),
]
