from __future__ import unicode_literals

from django.db import models

# Models.
class OwnerModel(models.Model):
    name = models.CharField(max_length=100)
    dni = models.IntegerField()
    address = models.CharField(max_length=200)
    phone = models.IntegerField()
    
    def __str__(self):
        return self.name

class DogModel(models.Model):
    name = models.CharField(max_length=50)
    age = models.IntegerField()
    breed = models.CharField(max_length=100)
    owner = models.ForeignKey(OwnerModel, null=True, on_delete=models.CASCADE)
