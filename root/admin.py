from django.contrib import admin

# Register your models here.
from root.models import DogModel
from root.models import OwnerModel


class DogModelAdmin(admin.ModelAdmin):
    pass
admin.site.register(DogModel, DogModelAdmin)

class OwnerModelAdmin(admin.ModelAdmin):
    pass
admin.site.register(OwnerModel, OwnerModelAdmin)
