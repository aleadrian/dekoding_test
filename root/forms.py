from django import forms
from root.models import DogModel
from root.models import OwnerModel

# DOG FORM
class AddDogForm(forms.ModelForm):
    # The selectMultiple widgets doesn't work well with ForeignKey
    owner = forms.ModelChoiceField(queryset=OwnerModel.objects.all(),initial=0)

    class Meta:
        model = DogModel
        fields = '__all__'

        widgets = {
            'name' : forms.TextInput(attrs={'class':'form-control input-form','placeholder':'Name','name':'name-dog'}),
            'age' : forms.NumberInput(attrs={'class':'form-control input-form','placeholder':'Age','name':'age-dog','pattern':'[0-9]','min':'1','max':'20'}),
            # a dog can live between 10 to 15 years, I considere 20 as age max that a dog can live.
            'breed' : forms.TextInput(attrs={'class':'form-control input-form','placeholder':'Breed','name':'breed-dog'}),
        }

# OWNER FORM
class AddOwnerForm(forms.ModelForm):
    class Meta:
        model = OwnerModel
        fields = '__all__'

        widgets = {
            'name' : forms.TextInput(attrs={'class':'form-control input-form','placeholder':'Name','name':'name-owner'}),
            'dni' : forms.NumberInput(attrs={'class':'form-control input-form','placeholder':'DNI','name':'dni-owner','pattern':'[0-9]','maxlength':'8','minlength':'8'}),
            # a dni have 8 digits.
            'address' : forms.TextInput(attrs={'class':'form-control input-form','placeholder':'Address','name':'address-owner'}),
            'phone' : forms.TextInput(attrs={'class':'form-control input-form','placeholder':'Phone','name':'phone-owner','pattern':'[9][0-9]{8}','maxlength':'9','minlength':'9'})
            # a phone have 9 digits and start with '9'.
        }